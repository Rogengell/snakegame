import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Apple {
    Circle circle;
    public Apple(int centerX, int centerY,int radius) {
        circle = new Circle(centerX,centerY,radius,new Color(1,0,0,1));
    }
    public Circle getCircle() {
        return circle;
    }



}
