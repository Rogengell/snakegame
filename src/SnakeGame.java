import javafx.animation.KeyFrame;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Random;

public class SnakeGame extends Application {

    final private int SCENE_WIDTH = 600;
    final private int SCENE_WIDTH_MINIMUM = 0;
    final private int SCENE_HEIGHT = 600;
    final private int SCENE_HEIGHT_MINIMUM = 0;
    final private int UNIT_SIZE = 20;
    final private int DELAY_IN_MILLIS = 100;
    final private int DELAY_IN_SECONDS_FOR_COUNTDOWN = 1;
    final private int COUNT_DOWN_TIME = 5;
    final private int APPLE_DELAY_TIMER_SECOND = 5;
    final private int MODULUS = 10;
    final private double speedADD = 0.025;

    final private int OBJECT_SIZE_WIDTH = (int)(SCENE_WIDTH / UNIT_SIZE);
    final private int OBJECT_SIZE_HEIGHT = (int)(SCENE_HEIGHT / UNIT_SIZE);

    final private Color HEAD_COLOR = new Color(0,0,0,1);
    final private Color BODY_COLOR = new Color(1,0.5,0,1);

    final private Color FIRST_COLOR_SQUARE = new Color(0,0.3,0,1);
    final private Color SECOND_COLOR_SQUARE = new Color(0,0.5,0,1);

    final private Color SPEED_APPLE = new Color(1,1,1,1);
    final private Color NORMAL_APPLE = new Color(1,0,0,1);

    final private Level LEVEL = new Level(OBJECT_SIZE_WIDTH, OBJECT_SIZE_HEIGHT, UNIT_SIZE,SCENE_WIDTH,SCENE_HEIGHT,FIRST_COLOR_SQUARE,SECOND_COLOR_SQUARE);

    final private int HEAD_INDEX = 0;
    final private int SECOND_BODY_INDEX = 1;

    private Stage stageGlobal;

    private ArrayList<SnakeBody> body;

    private Scene scene;
    private Scene start;
    private MoveDirection currentMove;
    private Label score;

    private Random random;
    private Apple apple;
    private Timeline tl;
    private Timeline tl2;
    private Timeline countDownTimer;
    private int countDown;
    private int moveAppleOrNot;
    private RotateTransition rotateTransition;
    private int eatenCount;
    private int rotateCounter;
    private double speed;
    private int highscore;

    public void newApple(){
        switch (random.nextInt(10)+1){
            case 1:
            case 2:
                apple.getCircle().setFill(SPEED_APPLE);
                break;
            default:
                apple.getCircle().setFill(NORMAL_APPLE);
                break;
        }

        moveAppleOrNot--;
        int appleX= random.nextInt(UNIT_SIZE)+1;
        int appleY= random.nextInt(UNIT_SIZE)+1;

        double randomTimesAppleX = ((OBJECT_SIZE_WIDTH*appleX)-(OBJECT_SIZE_WIDTH/2.0));
        double randomTimesAppleY = ((OBJECT_SIZE_HEIGHT*appleY)-(OBJECT_SIZE_HEIGHT/2.0));

        apple.getCircle().setLayoutX(randomTimesAppleX);
        apple.getCircle().setLayoutY(randomTimesAppleY);
    }

    public void moveWholeBody(int i){
        double x = body.get(i).getRectangle().getLayoutX();
        double y = body.get(i).getRectangle().getLayoutY();
        if (!(i == 0)) {
            body.get(i).getRectangle().setLayoutY(body.get(i-1).getRectangle().getLayoutY());
            body.get(i).getRectangle().setLayoutX(body.get(i-1).getRectangle().getLayoutX());
        }
        else if (i == 0) {
            switch (currentMove) {
                case UP:
                    body.get(i).getRectangle().setLayoutY(y - OBJECT_SIZE_HEIGHT);
                    break;
                case DOWN:
                     body.get(i).getRectangle().setLayoutY(y + OBJECT_SIZE_HEIGHT);
                     break;
                case LEFT:
                     body.get(i).getRectangle().setLayoutX(x - OBJECT_SIZE_WIDTH);
                     break;
                case RIGHT:
                     body.get(i).getRectangle().setLayoutX(x + OBJECT_SIZE_WIDTH);
                     break;
            }
        }
    }

    public void createSnakeBody(){
        SnakeBody tempSnakeBody = new SnakeBody(OBJECT_SIZE_WIDTH, OBJECT_SIZE_HEIGHT);
        tempSnakeBody.getRectangle().setFill(BODY_COLOR);

        tempSnakeBody.getRectangle().setLayoutX(body.get(body.size()-1).getRectangle().getLayoutX());
        tempSnakeBody.getRectangle().setLayoutY(body.get(body.size()-1).getRectangle().getLayoutY());

        body.add(tempSnakeBody);
        int bodyNewIndex = body.size();
        LEVEL.getPane().getChildren().addAll(body.get(bodyNewIndex-1).getRectangle());
        body.get(HEAD_INDEX).getRectangle().toFront();
    }

    public void createStartSnakeBody(){
        body.add(new SnakeBody(OBJECT_SIZE_WIDTH, OBJECT_SIZE_HEIGHT));
        body.add(new SnakeBody(OBJECT_SIZE_WIDTH, OBJECT_SIZE_HEIGHT));


        body.get(HEAD_INDEX).getRectangle().setFill(HEAD_COLOR);
        body.get(SECOND_BODY_INDEX).getRectangle().setFill(BODY_COLOR);


        LEVEL.getPane().getChildren().addAll(body.get(HEAD_INDEX).getRectangle());
        LEVEL.getPane().getChildren().addAll(body.get(SECOND_BODY_INDEX).getRectangle());


        body.get(HEAD_INDEX).getRectangle().setLayoutX(0);
        body.get(HEAD_INDEX).getRectangle().setLayoutY(0);
        body.get(SECOND_BODY_INDEX).getRectangle().setLayoutX(0);
        body.get(SECOND_BODY_INDEX).getRectangle().setLayoutY(0);

    }

    public void checkCollision(){
        if (body.get(HEAD_INDEX).getRectangle().getLayoutX() == (apple.getCircle().getLayoutX()-(OBJECT_SIZE_WIDTH/2.0)) && body.get(HEAD_INDEX).getRectangle().getLayoutY() == (apple.getCircle().getLayoutY()-(OBJECT_SIZE_HEIGHT/2.0))){
            createSnakeBody();

            if (apple.getCircle().getFill().equals(new Color(1, 1, 1, 1))) {
                ifWhiteAppleEaten();
            }

            if ((body.size()-2)%MODULUS == 0) {
                rotatePane();

            }

            newApple();
            eatenCount++;
            LEVEL.getLabel().setText("Score: " + eatenCount);

            for (int i = 0; i < body.size(); i++) {
                if (body.get(i).getRectangle().getLayoutX() == (apple.getCircle().getLayoutX()-(OBJECT_SIZE_WIDTH/2.0)) && body.get(i).getRectangle().getLayoutY() == (apple.getCircle().getLayoutY()-(OBJECT_SIZE_HEIGHT/2.0))){
                    newApple();
                    i=-1;
                }
            }

        }
        else if (body.get(HEAD_INDEX).getRectangle().getLayoutX() < SCENE_WIDTH_MINIMUM || body.get(HEAD_INDEX).getRectangle().getLayoutY() < SCENE_HEIGHT_MINIMUM){
            gameOver();
        }else if (body.get(HEAD_INDEX).getRectangle().getLayoutX() >= SCENE_WIDTH || body.get(HEAD_INDEX).getRectangle().getLayoutY() >= SCENE_HEIGHT){
            gameOver();
        }
        for (int i = 1; i < body.size()-1; i++) {
            if (body.get(HEAD_INDEX).getRectangle().getLayoutY() == body.get(i).getRectangle().getLayoutY() && body.get(HEAD_INDEX).getRectangle().getLayoutX() == body.get(i).getRectangle().getLayoutX()){
                gameOver();
            }
        }

    }

    public void ifWhiteAppleEaten(){
        tl.stop();
        speed += speedADD;
        tl.setRate(speed);
        tl.play();
    }

    public void createGame(){
        body = new ArrayList<SnakeBody>();
        currentMove = MoveDirection.RIGHT;
        moveAppleOrNot = 0;
        eatenCount = 0;
        rotateCounter = 1;
        speed = 1;

        LEVEL.getLabel().setText("Score: " + eatenCount);

        createStartSnakeBody();
        rotateTransition = new RotateTransition();
        rotateTransition.setNode(LEVEL.getPane());
        rotateTransition.setAutoReverse(false);
        rotateTransition.setByAngle(90);

        random = new Random();
        if (SCENE_WIDTH > SCENE_HEIGHT) {
            apple = new Apple(0, 0, (OBJECT_SIZE_HEIGHT / 2));
        }
        else if (SCENE_WIDTH < SCENE_HEIGHT) {
            apple = new Apple(0, 0, (OBJECT_SIZE_WIDTH / 2));
        }
        else{
            apple = new Apple(0, 0, (OBJECT_SIZE_WIDTH / 2));
        }
        LEVEL.getPane().getChildren().addAll(apple.getCircle());
        newApple();


        tl = new Timeline();
        tl.setCycleCount(Timeline.INDEFINITE);
        tl.setAutoReverse(false);
        tl.getKeyFrames().add(new KeyFrame(Duration.millis(DELAY_IN_MILLIS), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (int i = body.size()-1; i >= 0; i--) {
                    moveWholeBody(i);
                }
                checkCollision();
            }
        }));

        tl2 = new Timeline();
        tl2.setCycleCount(Timeline.INDEFINITE);
        tl2.setAutoReverse(false);
        tl2.getKeyFrames().add(new KeyFrame(Duration.seconds(APPLE_DELAY_TIMER_SECOND), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                moveAppleOrNot++;
                if (moveAppleOrNot == 1)
                {
                    newApple();
                }
                moveAppleOrNot = 0;
            }
        }));

        scene.setOnKeyPressed(keyEvent -> {
            String kp = keyEvent.getCode().toString();
            switch (kp) {
                case "W":
                    if (!currentMove.equals(MoveDirection.DOWN)) {
                        currentMove = MoveDirection.UP;
                    }
                    break;
                case "S":
                    if (!currentMove.equals(MoveDirection.UP)) {
                        currentMove = MoveDirection.DOWN;
                    }
                    break;
                case "D":
                    if (!currentMove.equals(MoveDirection.LEFT)) {
                        currentMove = MoveDirection.RIGHT;
                    }
                    break;
                case "A":
                    if (!currentMove.equals(MoveDirection.RIGHT)) {
                        currentMove = MoveDirection.LEFT;
                    }
                    break;
            }
        });

    }

    public void rotatePane(){
        rotateTransition.play();

        if (SCENE_WIDTH >= SCENE_HEIGHT) {
            switch (rotateCounter) {
                case 1:
                    LEVEL.getPane().setTranslateY(0);
                    LEVEL.getPane().setTranslateX(-((SCENE_WIDTH - SCENE_HEIGHT) / 2.0));
                    rotateCounter++;
                    break;
                case 2:
                    LEVEL.getPane().setTranslateY(-((SCENE_WIDTH - SCENE_HEIGHT) / 2.0));
                    LEVEL.getPane().setTranslateX(0);
                    rotateCounter++;
                    break;
                case 3:
                    LEVEL.getPane().setTranslateY(0);
                    LEVEL.getPane().setTranslateX((SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
                    rotateCounter++;
                    break;
                case 4:
                    LEVEL.getPane().setTranslateY((SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
                    LEVEL.getPane().setTranslateX(0);
                    rotateCounter = 1;
                    break;
            }
        }
        else {
            switch (rotateCounter) {
                case 1:
                    LEVEL.getPane().setTranslateY(-((SCENE_WIDTH - SCENE_HEIGHT) / 2.0));
                    LEVEL.getPane().setTranslateX(0);
                    rotateCounter++;
                    break;
                case 2:
                    LEVEL.getPane().setTranslateY(0);
                    LEVEL.getPane().setTranslateX((SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
                    rotateCounter++;
                    break;
                case 3:
                    LEVEL.getPane().setTranslateY((SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
                    LEVEL.getPane().setTranslateX(0);
                    rotateCounter++;
                    break;
                case 4:
                    LEVEL.getPane().setTranslateY(0);
                    LEVEL.getPane().setTranslateX(-(SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
                    rotateCounter = 1;
                    break;
            }
        }




    }

    public void gameOver(){
        tl.stop();
        tl2.stop();
        LEVEL.getPane().setRotate(0);
        if (SCENE_WIDTH >= SCENE_HEIGHT){
            LEVEL.getPane().setTranslateY((SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
            LEVEL.getPane().setTranslateX(0);
            score.setText(highscore(eatenCount));
        }
        else
        {
            LEVEL.getPane().setTranslateY(0);
            LEVEL.getPane().setTranslateX(-(SCENE_WIDTH - SCENE_HEIGHT) / 2.0);
            score.setText(highscore(eatenCount));
        }


        for (int i = 0; i < body.size(); i++) {
            LEVEL.getPane().getChildren().remove(body.get(i).getRectangle());
        }

        LEVEL.getPane().getChildren().remove(apple.getCircle());
        stageGlobal.setScene(start);
    }


    public void gameStart(){
        LEVEL.getCountDown().setOpacity(1);
        LEVEL.getCountDown().toFront();
        countDown = COUNT_DOWN_TIME;
        LEVEL.getCountDown().setText(String.valueOf(countDown));
        countDownTimer = new Timeline();
        countDownTimer.setCycleCount(Timeline.INDEFINITE);
        countDownTimer.setAutoReverse(false);
        countDownTimer.getKeyFrames().add(new KeyFrame(Duration.seconds(DELAY_IN_SECONDS_FOR_COUNTDOWN), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(countDown);
                if (countDown == 0){
                    tl.play();
                    tl2.play();
                    LEVEL.getCountDown().setOpacity(0);
                    LEVEL.getCountDown().toBack();
                    countDownTimer.stop();
                }
                countDown--;
                LEVEL.getCountDown().setText(String.valueOf(countDown));
            }
        }));
        countDownTimer.play();

    }

    public String highscore(int score){

        if(score > highscore){
            highscore = score;
        }
        else {
            highscore = highscore;
        }

        return ""+highscore;
    }

    @Override
    public void start(Stage stage) throws Exception {
        stageGlobal = stage;
        scene = new Scene(LEVEL.playField(), LEVEL.getBorderPane().getPrefWidth(), LEVEL.getBorderPane().getPrefHeight());
        scene.getStylesheets().add(super.getClass().getResource("CSS.css").toExternalForm());

        Pane pane = new Pane();
        pane.setId("startPane");
        start = new Scene(pane,LEVEL.getBorderPane().getPrefWidth(), LEVEL.getBorderPane().getPrefHeight());
        start.getStylesheets().add(super.getClass().getResource("CSS.css").toExternalForm());

        Button button = new Button();
        button.relocate(LEVEL.getBorderPane().getPrefWidth()/2-15,LEVEL.getBorderPane().getPrefHeight()-200);

        Label startLabel = new Label("start Game!");
        startLabel.setId("startLabel");
        startLabel.setPrefWidth(LEVEL.getBorderPane().getPrefWidth());
        startLabel.setLayoutY(LEVEL.getBorderPane().getPrefHeight()-225);
        //startLabel.relocate(LEVEL.getBorderPane().getPrefWidth()/2,LEVEL.getBorderPane().getPrefHeight()-225);

        Label lbSnakeGame = new Label("SNAKE GAME");
        lbSnakeGame.setId("snake");
        lbSnakeGame.setPrefWidth(LEVEL.getBorderPane().getPrefWidth());
        //lbSnakeGame.setLayoutY(LEVEL.getBorderPane().getPrefHeight()/2);

        Label highscore = new Label("current highscore");
        highscore.setId("score");
        highscore.setPrefWidth(LEVEL.getBorderPane().getPrefWidth());
        highscore.setLayoutY(LEVEL.getBorderPane().getPrefHeight()-(LEVEL.getBorderPane().getPrefHeight()-200));

        score = new Label();
        score.setId("score");
        score.setPrefWidth(LEVEL.getBorderPane().getPrefWidth());
        score.setLayoutY(LEVEL.getBorderPane().getPrefHeight()-(LEVEL.getBorderPane().getPrefHeight()-225));
        score.setText(highscore(eatenCount));


        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.setScene(scene);
                createGame();
                gameStart();
            }
        });

        pane.getChildren().addAll(button,startLabel,highscore,score,lbSnakeGame);

        stage.setTitle("Snake Game");
        stage.setResizable(false);
        stage.setScene(start);
        stage.show();
    }
}
