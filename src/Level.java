import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

public class Level {

    private BorderPane borderPane = new BorderPane();
    private Pane pane = new Pane();
    private Rectangle rectangle;
    private Label label;
    private Label countDown;

    public Level(int OBJECT_SIZE_WIDTH, int OBJECT_SIZE_HEIGHT, int UNIT_SIZE, int SCENE_WIDTH, int SCENE_HEIGHT, Color colorOne, Color colorTwo) {
        this.OBJECT_SIZE_WIDTH = OBJECT_SIZE_WIDTH;
        this.OBJECT_SIZE_HEIGHT = OBJECT_SIZE_HEIGHT;
        this.UNIT_SIZE = UNIT_SIZE;
        this.SCENE_WIDTH = SCENE_WIDTH;
        this.SCENE_HEIGHT = SCENE_HEIGHT;
        this.colorOne=colorOne;
        this.colorTwo=colorTwo;
    }

    final int OBJECT_SIZE_WIDTH;
    final int OBJECT_SIZE_HEIGHT;
    final int UNIT_SIZE;
    final int SCENE_WIDTH;
    final int SCENE_HEIGHT;
    final Color colorOne;
    final Color colorTwo;

    public BorderPane playField() {
        countDown = new Label();
        countDown.setId("countDown");
        int count = 0;

        for (int i = 0; i <UNIT_SIZE ; i++) {
            for (int j = 0; j <UNIT_SIZE ; j++) {
                rectangle = new Rectangle();
                if (count%2 == 0){
                    rectangle.setFill(colorOne);
                }
                else {
                    rectangle.setFill(colorTwo);
                }
                rectangle.setWidth(OBJECT_SIZE_WIDTH);
                rectangle.setHeight(OBJECT_SIZE_HEIGHT);
                rectangle.setLayoutX(j * OBJECT_SIZE_WIDTH);
                rectangle.setLayoutY(i * OBJECT_SIZE_HEIGHT);
                pane.getChildren().addAll(rectangle);
                count++;
            }
            count++;
        }

        int xValue = 0;
        int yValue = 0;


        for (int i = 0; i <= UNIT_SIZE; i++) {
            Line line = new Line();
            line.setStartX(xValue);
            line.setStartY(0);
            line.setEndX(xValue);
            line.setEndY(SCENE_HEIGHT);
            getPane().getChildren().addAll(line);
            xValue += OBJECT_SIZE_WIDTH;
        }

        for (int i = 0; i <= UNIT_SIZE; i++) {
            Line line = new Line();
            line.setStartX(0);
            line.setStartY(yValue);
            line.setEndX(SCENE_WIDTH);
            line.setEndY(yValue);
            getPane().getChildren().addAll(line);
            yValue += OBJECT_SIZE_HEIGHT;
        }


        label = new Label();
        label.setPrefHeight(OBJECT_SIZE_HEIGHT*2);
        label.relocate(0,0);
        label.setFont(new Font("Ink Free",35));
        label.toFront();
        label.setTextFill(Color.YELLOW);



        if (SCENE_WIDTH < SCENE_HEIGHT){
            borderPane.setMaxHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMaxWidth(SCENE_HEIGHT);
            borderPane.setMinHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMinWidth(SCENE_HEIGHT);
            borderPane.setPrefWidth(SCENE_HEIGHT);
            borderPane.setPrefHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));

            label.setPrefWidth(SCENE_HEIGHT);
            countDown.setPrefWidth(SCENE_HEIGHT);
            countDown.setPrefHeight(SCENE_HEIGHT);

            countDown.setLayoutX(-((SCENE_HEIGHT-SCENE_WIDTH)/2.0));

            countDown.setFont(new Font(SCENE_WIDTH/2.0));

            pane.setPrefHeight(SCENE_HEIGHT);
            pane.setPrefWidth(SCENE_HEIGHT);
            pane.setMaxWidth(SCENE_HEIGHT);
            pane.setMinWidth(SCENE_HEIGHT);
            countDown.setPrefHeight(SCENE_HEIGHT);

            pane.setTranslateX((SCENE_HEIGHT-SCENE_WIDTH)/2.0);

        }
        else if (SCENE_WIDTH > SCENE_HEIGHT) {
            borderPane.setMaxHeight(SCENE_WIDTH+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMaxWidth(SCENE_WIDTH);
            borderPane.setMinHeight(SCENE_WIDTH+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMinWidth(SCENE_WIDTH);
            borderPane.setPrefWidth(SCENE_WIDTH);
            borderPane.setPrefHeight(SCENE_WIDTH+(OBJECT_SIZE_HEIGHT*2));

            label.setPrefWidth(SCENE_WIDTH);

            countDown.setPrefWidth(SCENE_WIDTH);
            countDown.setPrefHeight(SCENE_WIDTH);

            countDown.setLayoutY(-((SCENE_WIDTH-SCENE_HEIGHT)/2.0));

            countDown.setFont(new Font(SCENE_WIDTH/2.0));

            pane.setPrefHeight(SCENE_WIDTH);
            pane.setPrefWidth(SCENE_WIDTH);
            pane.setMaxWidth(SCENE_WIDTH);
            pane.setMinWidth(SCENE_WIDTH);


            pane.setTranslateY((SCENE_WIDTH-SCENE_HEIGHT)/2.0);

        }
        else {
            borderPane.setMaxHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMaxWidth(SCENE_WIDTH);
            borderPane.setMinHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));
            borderPane.setMinWidth(SCENE_WIDTH);
            borderPane.setPrefWidth(SCENE_WIDTH);
            borderPane.setPrefHeight(SCENE_HEIGHT+(OBJECT_SIZE_HEIGHT*2));

            label.setPrefWidth(SCENE_WIDTH);
            countDown.setPrefWidth(SCENE_WIDTH);
            countDown.setPrefHeight(SCENE_HEIGHT);

            countDown.relocate(0,0);

            countDown.setFont(new Font(SCENE_WIDTH/2.0));

            pane.setPrefHeight(SCENE_HEIGHT);
            pane.setPrefWidth(SCENE_HEIGHT);
            pane.setMaxWidth(SCENE_HEIGHT);
            pane.setMinWidth(SCENE_HEIGHT);

        }
        borderPane.setTop(label);
        borderPane.setCenter(pane);
        borderPane.setStyle("-fx-background-color: GREEN");

        pane.getChildren().addAll(countDown);
        return borderPane;
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

    public Pane getPane() {
        return pane;
    }

    public Label getLabel() {
        return label;
    }

    public Label getCountDown() {
        return countDown;
    }

}
